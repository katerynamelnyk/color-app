import { ChangeEventHandler, FormEvent, useEffect, useState } from 'react';

import styles from './FormToAddColor.module.scss';


const FormToAddColor: React.FC<{ onAddToList: Function }> = (props) => {

    const [wrongColor, setWrongColor] = useState(false);
    const [color, setColor] = useState('');

    const submitColorHandler = (event: FormEvent | any) => {
        event.preventDefault();

        if (!wrongColor && event.target[0].value !== '') {

            let currentKey = 0;
            let keys: string[] = Array.from(Object.keys(localStorage));

            for (let i = 0; i < keys.length; i++) {
                if (+keys[i] > +currentKey) {
                    currentKey = +keys[i];
                }
            }
            currentKey++;
            localStorage.setItem(currentKey.toString(), color.toUpperCase());
            setColor('');

            props.onAddToList(true);
        }
    }

    const changeColorHandler: ChangeEventHandler<HTMLFormElement> = (event) => {
        setColor(prev => event.target.value);
    }

    useEffect(() => {
        const regex = /^#(?:[0-9a-fA-F]{6})$/i;
        if (color !== '' && !color.match(regex)) {
            setWrongColor(true);
        } else {
            setWrongColor(false);
        }
    }, [color])


    return <form className={styles.form} onChange={changeColorHandler} onSubmit={submitColorHandler}>
        <label>You can add new color: </label>
        <input placeholder='use HEX form' value={color}></input>
        {wrongColor ?
            <button type='submit' disabled value="submit">SUBMIT</button> :
            <button  type='submit' value="submit">SUBMIT</button>}
        {wrongColor ? <div className={styles.message}>Color format is incorrect!</div> : <div></div>}
    </form>
}


export default FormToAddColor;