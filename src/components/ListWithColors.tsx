
import styles from './ListWithColors.module.scss';

const ListWithColors: React.FC<{ hex: string, custom: boolean, colorID: string, onRemoveFromList: Function }> = (props) => {

    const clickHandler = (event: any) => {

        let key = event.currentTarget.value;
        localStorage.removeItem(key);
        props.onRemoveFromList(true);
    }


    return <div className={styles.wrapper}>
        <div
            className={styles.rectangle}
            style={{ backgroundColor: `${props.hex}` }}
        ></div>
        <p className={styles.text}>{props.hex}
            {props.custom ? <button value={props.colorID} onClick={clickHandler}>x</button> : ''}
        </p>

    </div >
}


export default ListWithColors;