import { ChangeEventHandler, useEffect, useState } from 'react';
import styles from './FormToFilterColors.module.scss';
import { rgbData } from '../App';

const FormToFilterColors: React.FC<{onFilter:Function}> = (props) => {

    const [red, setRed] = useState(0);
    const [green, setGreen] = useState(0);
    const [blue, setBlue] = useState(0);
    const [saturation, setSaturation] = useState(0);

    const changeValueHandler: ChangeEventHandler<HTMLFormElement> = (event) => {

        if(event.target.id === 'red') {
            setRed(prev => event.target.value);
        } else if (event.target.id === 'green') {
            setGreen(prev => event.target.value);
        } else if (event.target.id === 'blue') {
            setBlue(prev => event.target.value);
        } else if (event.target.id === 'saturation') {
            setSaturation(prev => event.target.value);
        }
    }

    useEffect(() => {
        const data: rgbData = {
            redValue: red,
            greenValue: green,
            blueValue: blue,
            saturationValue: saturation
        }
        props.onFilter(data);
    }, [red, green, blue, saturation, props])


    return <form className={styles.form} onChange={changeValueHandler}>
        <h4>What colors do you want to show?</h4>
        <div>
            <label>Red (between 0 and 255)</label>
            <input type="range" min="0" max="255" id="red" defaultValue={red}></input>
        </div>
        <div>
            <label>Green (between 0 and 255)</label>
            <input type="range" min="0" max="255" id="green" defaultValue={green}></input>
        </div>
        <div>
            <label>Blue (between 0 and 255)</label>
            <input type="range" min="0" max="255" id="blue" defaultValue={blue}></input>
        </div>
        <div>
            <label>Saturation (between 0 and 100)</label>
            <input type="range" min="0" max="100" id="saturation" defaultValue={saturation}></input>
        </div>

    </form>
}


export default FormToFilterColors;