import { useEffect, useState } from 'react';
import styles from './App.module.scss';
import FormToAddColor from './components/FormToAddColor';
import FormToFilterColors from './components/FormToFilterColors';
import ListWithColors from './components/ListWithColors';


export type color = { hex: string, red: number, green: number, blue: number, saturation: number, id: string }
export type rgbData = { redValue: number, greenValue: number, blueValue: number, saturationValue: number }


const all: color[] = [
  { hex: '#0000FF', red: 0, green: 0, blue: 255, saturation: 100, id: 'd3' },
  { hex: '#FFFF00', red: 255, green: 255, blue: 0, saturation: 100, id: 'd4' },
  { hex: '#FF0000', red: 255, green: 0, blue: 0, saturation: 100, id: 'd1' },
  { hex: '#00FF00', red: 0, green: 255, blue: 0, saturation: 100, id: 'd2' },
  { hex: '#000000', red: 0, green: 0, blue: 0, saturation: 0, id: 'd5' },
  { hex: '#FFFFFF', red: 255, green: 255, blue: 255, saturation: 0, id: 'd6' },
  { hex: '#00FFFF', red: 0, green: 255, blue: 255, saturation: 100, id: 'd7' },
  { hex: '#FF00FF', red: 255, green: 0, blue: 255, saturation: 100, id: 'd8' },
]

const App = () => {

  const getColorsFromLocalStorage = (): color[] => {
    const customColorsHEX: color[] = [];
    let keys: string[] = Array.from(Object.keys(localStorage));
    keys.forEach(key => {
      let item = localStorage.getItem(`${key}`);
      if (item !== undefined && item !== null) {
        let hex: string = item;
        let red = parseInt((item.substring(1, 3)), 16);
        let green = parseInt((item.substring(3, 5)), 16);
        let blue = parseInt((item.substring(5, 7)), 16);

        let r = red / 255;
        let g = green / 255;
        let b = blue / 255;
        let max = Math.max(r, g, b);
        let min = Math.min(r, g, b);
        let luminosity = 1 / 2 * (max + min);
        let saturation = Math.round((max - min) / (1 - Math.abs(2 * luminosity - 1)) * 100);

        let obj: color = { hex, red, green, blue, saturation, id: key };
        customColorsHEX.push(obj);
      }
    });
    return customColorsHEX;
  }


  const [filteredColors, setFilteredColors] = useState(all);
  const [filteredCustomColors, setFilteredCustomColors] = useState(getColorsFromLocalStorage());

  const [red, setRed] = useState(0);
  const [green, setGreen] = useState(0);
  const [blue, setBlue] = useState(0);
  const [saturation, setSaturation] = useState(0);

  const [changeCustomColor, setChangeCustomColor] = useState(false);


  useEffect(() => {
    setFilteredColors(prev => {
      prev.sort((a: color, b: color) => b.blue - a.blue);
      prev.sort((a: color, b: color) => b.green - a.green);
      prev.sort((a: color, b: color) => b.red - a.red);
      return prev;
    })
    setFilteredCustomColors(prev => {
      prev.sort((a: color, b: color) => b.blue - a.blue);
      prev.sort((a: color, b: color) => b.green - a.green);
      prev.sort((a: color, b: color) => b.red - a.red);
      return prev;
    })
  }, [red, green, blue, filteredColors, filteredCustomColors])


  const filterHandler = (data: rgbData) => {
    setRed(prev => data.redValue);
    setGreen(prev => data.greenValue);
    setBlue(prev => data.blueValue);
    setSaturation(prev => data.saturationValue);
  }


  useEffect(() => {
    setFilteredColors(all)
    setFilteredColors(prev => {
      const result: color[] = prev.filter(element => element.red >= +red &&
        element.green >= +green && element.blue >= +blue && element.saturation >= +saturation)
      return result;
    })

    setFilteredCustomColors(getColorsFromLocalStorage())
    setFilteredCustomColors(prev => {
      const result: color[] = prev.filter(element => element.red >= +red &&
        element.green >= +green && element.blue >= +blue && element.saturation >= +saturation)
      return result;
    })

  }, [red, green, blue, saturation])


  const addToListHandler = (addToList: boolean) => {
    setChangeCustomColor(addToList);
  }

  const removeFromListHandler = (removeFromList: boolean) => {
    setChangeCustomColor(removeFromList);
  }

  useEffect(() => {
    const colorsFromStorage = getColorsFromLocalStorage();
    setFilteredCustomColors([]);
    colorsFromStorage.forEach(element => {
      setFilteredCustomColors(oldArray => [...oldArray, element]);
    })
    setChangeCustomColor(false);
  }, [changeCustomColor])


  return (
    <div className={styles.mainWrapper}>
      <div className={styles.wrapperForForms}>
        <FormToAddColor onAddToList={addToListHandler}></FormToAddColor>
        <FormToFilterColors onFilter={filterHandler}></FormToFilterColors>
      </div>
      <div className={styles.infoBlock}>
        {`red:${red} green:${green} blue:${blue} saturation:${saturation}`}
      </div>
      <div className={styles.wrapperForList}>
        {filteredColors.map(color =>
          <ListWithColors
            hex={color.hex} key={color.id} custom={false} colorID={color.id} onRemoveFromList={removeFromListHandler}
          ></ListWithColors>)}
        {filteredCustomColors.map(color =>
          <ListWithColors
            hex={color.hex} key={color.id} custom={true} colorID={color.id} onRemoveFromList={removeFromListHandler}
          ></ListWithColors>)}
      </div>
    </div>
  );
}

export default App;
